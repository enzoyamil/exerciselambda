import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.DoubleToIntFunction;

public class Main {
    public static void main(String[] args){
        FuncionesLambda fl = new FuncionesLambda();
        //Exercise 1
        ArrayList<String> listOfStrings= new ArrayList<>();
        listOfStrings.add("AAA");
        listOfStrings.add("BB");
        listOfStrings.add("CCC");
        listOfStrings.add("D");
        listOfStrings.add("DDD");
        listOfStrings.add("JJJ");
        listOfStrings.add("G");
        listOfStrings.add("T");
        List<String> listResult= new ArrayList<>();
        listResult = fl.search(listOfStrings);
        fl.PrintList(listResult);
        //Exercise 2
        ArrayList<Integer> listNumber= new ArrayList<>();
        listNumber.add(6);
        listNumber.add(1);
        listNumber.add(44);
        listNumber.add(3);
        String result = fl.getString(listNumber);
        System.out.println(result);
        //Exercise one
        ArrayList<String> listOfWords= new ArrayList<>();
        listOfWords.add("HOLA");
        listOfWords.add("MUNDO");
        listOfWords.add("HOLA");
        listOfWords.add("MUNDO");
        String res = fl.exercise1(listOfWords);
        System.out.println(res);
        //Exercise two
        System.out.println("-----Lista de Tamaño Par-----");
        fl.exercise2(listOfWords);
        fl.PrintList(listOfWords);
        //Exercise three
        System.out.println("-----Lista en minusculas-----");
        fl.exercise3(listOfWords);
        //Exercise for
        HashMap<String,Integer> map = new HashMap<String,Integer>();
        map.put("key1",1);
        map.put("key2",2);
        map.put("key3",3);
        fl.exercise4(map);

        //exercisefive
        ArrayList<Integer> arr = new ArrayList<>();
        System.out.println("Exercise 5");
        arr.add(1);
        arr.add(2);
        arr.add(3);
        arr.add(4);
        arr.add(5);
        arr.add(6);
        arr.add(7);
        arr.add(8);
        PrintStream out = new PrintStream(System.out);
        fl.exercise5(arr,out);
    }
}



