import javax.swing.text.AbstractDocument;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FuncionesLambda {

    public FuncionesLambda() {

    }

    //Imprimir Lista
    public void PrintList(List<String> lista) {
        lista.forEach(System.out::println);
    }

    /*Dada un lista de cadenas escribe un metodo que retorne una lista de todas las cadenas
     * que inicie con la letra 'a'(Minuscula) y tengan exactamente 3 letras*/
    public List<String> search(List<String> list) {
        ArrayList<String> listResult = new ArrayList<>();
        list
                .stream()
                .filter(element -> element.length() == 3)
                .forEach(element -> listResult.add(element));
        return listResult;
    }

    /*escriba un método que devuelva una cadena separada por comas basada en una lista dada de enteros.
    cada elemento debe estar precedido por la letra 'e' si el número es par, y precedido por la letra 'o' si el número es impar.
    por ejemplo, si la lista de entrada es (3,44), la salida debería ser 'o3,e44'.
    * */
    public String getString(List<Integer> list) {
        ArrayList<String> listRes = new ArrayList<>();
        String res = "";
        String par = "e";
        String impar = "o";
        list.stream().forEach(element -> {
            //String result = "";
            if (element % 2 == 0)
                listRes.add(par + Integer.toString(element) + ",");
            else {
                listRes.add(impar + Integer.toString(element) + ",");
            }
        });
        res = converString(listRes);
        System.out.println(res);
        return res;
    }

    private String converString(List<String> list) {
        String res = "";
        res = list.stream().reduce("", ((s1, s2) -> s1 + "" + s2));
        return res;
    }

    /*cree una cadena que consista en la primera letra de cada palabra en la lista de cadenas proporcionada.
    la lista no está vacía. Las cadenas en la lista no son nulas o la cadena vacía
    * */
    public String exercise1(List<String> list) {
        String res = "";
        ArrayList<String> listRes = new ArrayList<>();
        list.stream().forEach(element -> listRes.add(Character.toString(element.charAt(0))));
        res = converString(listRes);
        return res;
    }

    /*elimine las palabras que tienen longitudes impares de la lista. cambie la lista de entrada.*/
    public void exercise2(List<String> list) {
        list.removeIf(element -> element.length() % 2 != 0);
    }

    /*reemplace cada palabra en la lista con su equivalente en mayúsculas. cambiar la lista de entrada.
     la lista no está vacía. las cadenas en la lista no son nulas o la cadena vacía*/
    public void exercise3(List<String> list) {
        list = list.stream().map(String::toLowerCase).collect(Collectors.toList());
        PrintList(list);
    }

    /*
     * convertir cada par clave-valor del mapa en una cadena y agregarlos todos en una sola cadena, en orden de iteración.
     *la salida debe seguir el formato: "key1=1,key2=2,key3=3".
     * la entrada nunca está vacía y las claves no son nulas o la cadena vacía.*/
    public String exercise4(Map<String, Integer> map) {
        String res = "";
        ArrayList<String> listString = new ArrayList<>();
        map.forEach((k, v) -> listString.add(k.toString()+"="+ v +","));
        res = converString(listString);
        System.out.println(res);
        return res;
    }
    /*cree un nuevo hilo que imprima los números de la lista. imprime la salida en el argumento 'out' en lugar de system.out.
     *la lista nunca es nula, está vacía o contiene valores nulos.
     *los números impresos deben estar separados por un carácter de espacio en blanco.*/

   public void exercise5(List<Integer>list, PrintStream out){
           list.stream().forEach(element-> {
               out.println(element.toString());
           });
       out.close();
   }

}